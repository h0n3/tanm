#include <array>
#include <functional>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <thread>
#include <atomic>

#include "threshold_functors.hpp"
#include "simplex/nelder-mead.hpp"
#include "simplex/simplex_shift.hpp"
#include "simplex/simplex_functor_weighted.hpp"
#include "metaheuristics.hpp"

template <std::size_t dimension, class value_t, std::size_t rounds>
struct tanm_search_params {
    using thresholds_t      = std::array<value_t, rounds>;
    using iterations_t      = std::array<std::size_t, rounds>;
    using p_simplex_shift_t = std::array<value_t, rounds>;

    using a_nm_params_t                  = nm_params<value_t>;
    using a_simplex_shift_params_entry_t = simplex_shift_params<dimension, value_t>;
    using a_simplex_shift_params_list_t  = std::array<a_simplex_shift_params_entry_t, rounds>;

    // Global
    thresholds_t thresholds;
    iterations_t iterations;

    p_simplex_shift_t p_simplex_shift;

    // Nelder-Mead
    a_nm_params_t nm;

    // simplex shift
    a_simplex_shift_params_list_t simplex_shift;
};

template <std::size_t dimension,
    class value_t,
    function_t<dimension, value_t> function,
    std::size_t                    rounds,
    class random_device_t,
    class threshold_functor_t>
class tanm_search {
public:
    using a_simplex_vector_t   = simplex_vector_t<dimension, value_t>;
    using a_simplex_t          = simplex_t<dimension, value_t>;
    using a_tanm_search_params = tanm_search_params<dimension, value_t, rounds>;

    struct search_result {
        a_simplex_vector_t minimum;

        std::size_t round;
        std::size_t iterations;

        search_result()
            : round{ 0 }
            , iterations{ 0 }
        {
        }

        search_result(a_simplex_vector_t minimum,
            std::size_t                  number_of_iterations,
            std::size_t                  calculation_round)
            : minimum{ minimum }
            , round{ calculation_round }
            , iterations{ number_of_iterations }
        {
        }
    };

private:
    a_tanm_search_params _params;

    random_device_t _random_device;

    a_simplex_t _simplex;

public:
    tanm_search(a_tanm_search_params params, random_device_t& random_device, a_simplex_t& simplex)
        : _params{ params }
        , _random_device{ std::move(random_device) }
        , _simplex{ std::move(simplex) }
    {
    }

    // FIXME like this the algo might walk away from a minimum and doesnt keep track of it.
    search_result perform_search()
    {
        std::size_t sum_iterations{ 0 };

        sort_simplex<dimension, value_t>(_simplex);
        std::uniform_real_distribution<value_t> method_dist{ 0.0, 1.0 };

        a_simplex_vector_t minimum{ _simplex.front() };
        a_simplex_t        best_simplex{ _simplex };

        for (std::size_t round = 0; round < rounds; ++round) {
            const std::size_t   iterations = _params.iterations[round];
            threshold_functor_t threshold_functor{ _params.thresholds[round] };

            if (round) {
                _simplex = best_simplex;
            }

            const std::size_t no_champion_freedom_iterations
                = (round < 0) ? iterations / (5000 / (round + 1)) : iterations;
            const std::size_t no_improvement_freedom_iterations
                = (round < 0) ? iterations / (15000 / (round + 1)) : iterations;
            const std::size_t max_resets{ 15 / (round + 1) };

            std::size_t iterations_without_improvement{ 0 };
            std::size_t iterations_without_champion{ 0 };
            std::size_t resets{ 0 };

            const value_t p_simplex_shift = _params.p_simplex_shift[round];
            value_t       last_func_val{ _simplex.front().function_value };

            std::size_t iteration;
            for (iteration = 0; iteration < iterations; ++iteration) {
                value_t func_val;

                const value_t method_value{ method_dist(_random_device) };
                if (method_value > p_simplex_shift) {
                    func_val = perform_simplex_shift<dimension,
                        value_t,
                        function,
                        random_device_t,
                        threshold_functor_t>(
                        _params.simplex_shift[round], _simplex, _random_device, threshold_functor);
                } else {
                    auto iteration_result
                        = perform_nm_iteration<dimension, value_t, function, threshold_functor_t>(
                            _params.nm, _simplex, threshold_functor);

                    func_val = iteration_result.best_val;
                }

                if (func_val < last_func_val) {
                    iterations_without_improvement++;
                } else {
                    iterations_without_improvement = 0;
                }

                if (func_val <= minimum.function_value) // found new champion
                {
                    best_simplex = _simplex;
                    minimum      = _simplex.front();

                    iterations_without_improvement = 0;
                    iterations_without_champion    = 0;
                } else {
                    ++iterations_without_champion;
                }

                if ((iterations_without_improvement > no_improvement_freedom_iterations)
                    || (iterations_without_champion > no_champion_freedom_iterations)) {
                    _simplex                       = best_simplex;
                    iterations_without_improvement = 0;
                    iterations_without_champion    = 0;

                    if (++resets > max_resets) {
                        break;
                    }
                }

                last_func_val = func_val;
            }

            sum_iterations += iteration;
        }
        return search_result{ minimum, sum_iterations, 0u };
    }
};

using coord_t = vector_t<2, double>;

template <int a, int b>
double rosenbrook_function_2d(const coord_t& coord)
{
    const double& x = coord[0];
    const double& y = coord[1];
    return std::pow(static_cast<double>(a) - x, 2.0)
        + static_cast<double>(b) * pow((y - pow(x, 2.0)), 2.0);
}

using tanm_search_2d = tanm_search<2,
    double,
    rosenbrook_function_2d<1, 100>,
    6u,
    std::mt19937,
    linear_threshold_functor<double>>;

using a_simplex_t = simplex_t<2, double>;

template <function_t<2, double> function>
a_simplex_t
generate_simplex(double x_min, double x_max, double y_min, double y_max, double off_min, double off_max)
{
    using a_vector_t = vector_t<2, double>;
    std::random_device         r;
    std::default_random_engine e(r());

    std::uniform_real_distribution<double> off_dist{ off_min, off_max };
    double                                 off_distance = off_dist(e);

    a_vector_t offset{ off_dist(e), off_dist(e) };
    double     length = std::sqrt(std::pow(offset.front(), 2.0) + std::pow(offset.back(), 2.0));
    mul_scalar_vector<double, 2>((1.0 / length) * off_distance, offset);

    length = std::sqrt(std::pow(offset.front(), 2.0) + std::pow(offset.back(), 2.0));

    std::uniform_real_distribution<double> x_dist(x_min, x_max);
    std::uniform_real_distribution<double> y_dist(y_min, y_max);

    a_simplex_t result;

    for (auto& entry : result) {
        entry.vertice = { x_dist(e), y_dist(e) };
        add_vector_vector<double, 2>(entry.vertice, offset);

        entry.function_value = function(entry.vertice);
    }

    return result;
}

std::ostream& operator<<(std::ostream& stream, const tanm_search_2d::search_result& result)
{
    stream << "Simplex search after " << result.iterations << " Iterations:\n";
    stream << "Minimum:" << result.minimum.function_value << " at [ ";
    for (auto cord : result.minimum.vertice) {
        stream << cord << ' ';
    }
    stream << "]\n";

    return stream;
}

void perform_try(const tanm_search_2d::a_tanm_search_params& params,
    std::atomic<std::size_t>&                                total_runs,
    std::atomic<std::size_t>&                                total_iterations)
{
    std::random_device r;

    std::size_t                   runs{ 0 };
    std::size_t                   iterations{ 0 };
    tanm_search_2d::search_result result;
    do {
        auto start_simplex = generate_simplex<rosenbrook_function_2d<1, 100>>(
            -30.0, 30.0, -30.0, 30.0, 30000.0, 100000.0);

        std::mt19937   rand_engine(r());
        tanm_search_2d sim_search(params, rand_engine, start_simplex);

        result = sim_search.perform_search();

        ++runs;
        iterations += result.iterations;
    } while (result.minimum.function_value > 0.1);

    std::cout << "finished after " << runs << " runs "
              << "and a total of " << iterations << " iterations" << std::endl;
    std::cout << "with result:\n" << result;

    total_runs += runs;
    total_iterations += iterations;
}

int main()
{
    using tanm_sp_t        = tanm_search_2d::a_tanm_search_params;
    using sshift_params_t  = tanm_search_2d::a_tanm_search_params::a_simplex_shift_params_entry_t;
    using shift_distance_t = sshift_params_t::shift_distance_t;

    tanm_sp_t params{};

    params.nm = tanm_sp_t::a_nm_params_t{ 1.0, 2.0, 0.5, 0.5 };

    params.simplex_shift = tanm_sp_t::a_simplex_shift_params_list_t{
        sshift_params_t{ shift_distance_t{ -50000.0, 50000.0 }, shift_distance_t{ -50000.0, 50000.0 } },
        sshift_params_t{ shift_distance_t{ -5000.0, 5000.0 }, shift_distance_t{ -5000.0, 5000.0 } },
        sshift_params_t{ shift_distance_t{ -500.0, 500.0 }, shift_distance_t{ -500.0, 500.0 } },
        sshift_params_t{ shift_distance_t{ -50, 50 }, shift_distance_t{ -50, 50 } },
        sshift_params_t{ shift_distance_t{ -0.1, 0.1 }, shift_distance_t{ -0.1, 0.1 } },
        sshift_params_t{ shift_distance_t{ -0.1, 0.1 }, shift_distance_t{ -0.1, 0.1 } }
    };

    params.p_simplex_shift
        = tanm_search_2d::a_tanm_search_params::p_simplex_shift_t{ 0.1, 0.1, 0.2, 0.7, 0.8, 0.9 };
    params.thresholds
        = tanm_search_2d::a_tanm_search_params::thresholds_t{ 1.1, 1.1, 1.05, 1.03, 1.01, 1.0 };
    params.iterations
        = tanm_search_2d::a_tanm_search_params::iterations_t{ 50000, 20000, 10000, 5000, 2000, 2000 };

    const int num_threads = 10;
    const int num_trys    = 100;

    std::atomic<std::size_t> total_runs{ 0 };
    std::atomic<std::size_t> total_iterations{ 0 };
    std::atomic<std::size_t> total_trys{ 0 };

    std::vector<std::thread> threads{};
    for (std::size_t thread = 0; thread < num_threads; ++thread) {
        threads.emplace_back([&params, &total_runs, &total_iterations, &total_trys]() {
            while (++total_trys < num_trys) {
                perform_try(params, total_runs, total_iterations);
            }
        });
    }

    for (auto& thread : threads) {
        thread.join();
    }

    double mean_runs       = static_cast<double>(total_runs) / static_cast<double>(total_trys);
    double mean_iterations = static_cast<double>(total_iterations) / static_cast<double>(total_trys);
    std::cout << "____________________________________________________________" << std::endl;
    std::cout << "did " << total_trys << " evaluations" << std::endl;
    std::cout << "total number of runs " << total_runs << " with a mean of " << mean_runs
              << " runs per solve" << std::endl;
    std::cout << "total number of iterations " << total_iterations << " with a mean of "
              << mean_iterations << " iterations per solve" << std::endl;

    return 0;
}
