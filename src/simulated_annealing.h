//
// Created by h on 22.09.19.
//

#ifndef H_META_SIMULATED_ANNEALING_H
#define H_META_SIMULATED_ANNEALING_H

#include "h_meta.hpp"

template <std::size_t dimension, class value_t>
struct simulated_annealing_params {
};

template <std::size_t dimension, class value_t>
value_t perform_simulated_annealing_iteration(vector_t<dimension, value_t>& active_state, )
{
    return 0.0;
}

#endif // H_META_SIMULATED_ANNEALING_H
