//
// Created by dvl on 2/19/20.
//

#ifndef H_METAHEURISTICS_THRESHOLD_FUNCTORS_HPP
#define H_METAHEURISTICS_THRESHOLD_FUNCTORS_HPP

template <class value_t>
class default_threshold_functor {
public:
    value_t operator()(const value_t& value)
    {
        return value;
    }
};

template <class value_t>
class linear_threshold_functor {
    value_t _factor;

public:
    linear_threshold_functor(value_t factor)
        : _factor{ factor }
    {
    }

    value_t operator()(const value_t& value)
    {
        return value * _factor;
    }
};

#endif // H_METAHEURISTICS_THRESHOLD_FUNCTORS_HPP
