//
// Created by dvl on 2/20/20.
//

#ifndef H_METAHEURISTICS_TUPLE_HELPERS_HPP
#define H_METAHEURISTICS_TUPLE_HELPERS_HPP

#include <tuple>

template <class resolver_t, class... parameter_pack_t>
class tuple_by_id_index {
    using tuple_t = std::tuple<parameter_pack_t...>;

    template <std::size_t length, class... functor_arguments_t>
    static void __execute_at(size_t id, tuple_t& tuple, functor_arguments_t... args)
    {
        if (id == length) {
            auto entry    = std::get<length>(tuple);
            auto mem_func = resolver_t::resolve(entry);
            (entry.*mem_func)(args...);
            return;
        }
        if constexpr (length) {
            __execute_at<length - 1, functor_arguments_t...>(id, tuple, args...);
            return;
        }
    }

public:
    template <class... functor_arguments_t>
    static void execute_at(size_t id, tuple_t& tuple, functor_arguments_t... args)
    {
        __execute_at<sizeof...(parameter_pack_t) - 1, functor_arguments_t...>(id, tuple, args...);
    }
};

template <std::size_t id, std::size_t offset>
static std::size_t get_entry_helper(std::size_t value)
{
    throw "erorr";
}

template <std::size_t id, std::size_t offset, class param_t, class... param_pack_t>
static std::size_t get_entry_helper(std::size_t value)
{
    constexpr const std::size_t off{ offset + param_t::weight };
    if (value <= off) {
        return id;
    }
    return get_entry_helper<id + 1, off, param_pack_t...>(value);
}

template <class... param_pack_t>
static std::size_t get_entry_id(std::size_t value)
{
    return get_entry_helper<0u, 0u, param_pack_t...>(value);
}

#endif // H_METAHEURISTICS_TUPLE_HELPERS_HPP
