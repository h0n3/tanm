//
// Created by dvl on 3/9/20.
//

#ifndef H_METAHEURISTICS_PRINT_METAHEURISTICS_HPP
#define H_METAHEURISTICS_PRINT_METAHEURISTICS_HPP

#include "../../metaheuristics.hpp"

#include <iostream>

/**
 * \brief Print vector_t
 */
template <std::size_t dimension, class value_t>
void print_vector_t(std::ostream& os, const vector_t<dimension, value_t>& vec)
{
    os << "vector_t[" << dimension << "]( ";
    for (const value_t& elem : vec) {
        os << std::to_string(elem) << ' ';
    }
    os << ")\n";
}

/**
 * \brief Print iteration_result
 */
template <std::size_t dimension, class value_t>
void print_iteration_result(std::ostream& os, const iteration_result<dimension, value_t>& ir)
{
    os << "iteration_result[" << dimension << "] (\n";
    os << '\t';
    print_vector_t(os, ir.point);
    os << '\t' << "Value: " << std::to_string(ir.f_point) << "\n";
}

#endif // H_METAHEURISTICS_PRINT_METAHEURISTICS_HPP
