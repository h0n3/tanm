//
// Created by dvl on 3/9/20.
//

#ifndef H_METAHEURISTICS_PRINT_SIMPLEX_HPP
#define H_METAHEURISTICS_PRINT_SIMPLEX_HPP

#include "../../simplex/simplex.hpp"

/**
 * \brief Print simplex_vector_t
 */
template <std::size_t dimension, class value_t>
void print_simplex_vector(std::ostream& os, const simplex_vector_t<dimension, value_t>& ir)
{
    os << "simplex_vector[" << dimension << "] (\n";
    os << '\t';
    print_vector_t(os, ir.point);
    os << '\t' << "Value: " << std::to_string(ir.f_point) << "\n";
}

#endif // H_METAHEURISTICS_PRINT_SIMPLEX_HPP
