#ifndef H_H_META_HELPER_FUNCS_HPP
#define H_H_META_HELPER_FUNCS_HPP

#include <array>

template <class data_t, class parent_t, data_t (parent_t::*eval_func)()>
class lazy_evaluated_value {
    parent_t& _parent;
    bool      _evaluated;
    data_t    _value;

public:
    lazy_evaluated_value(parent_t& parent)
        : _parent{ parent }
        , _evaluated{ false }
    {
    }

    data_t& get()
    {
        if (!_evaluated) {
            _value     = eval_func();
            _evaluated = true;
        }
        return _value;
    }
};

#endif // H_H_META_HELPER_FUNCS_HPP