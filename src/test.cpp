//
// Created by dvl on 2/27/20.
//

#include "simplex/simplex.hpp"
#include "simplex/simplex_functor_weighted.hpp"
#include "simplex/nelder-mead.hpp"
#include "simplex/simplex_shift.hpp"
#include "metaheuristics.hpp"

#include "termination/termination.hpp"

#include "threshold_functors.hpp"

#include "util/print/metaheuristics.hpp"
#include "util/print/simplex.hpp"

#include <random>

const std::size_t dimensions = 2;
using a_value_t              = double;

using a_vector_t  = vector_t<dimensions, a_value_t>;
using a_simplex_t = simplex_t<dimensions, a_value_t>;

using a_iteration_result_t = iteration_result<dimensions, a_value_t>;

using a_simplex_shift_functor_t = simplex_shift_functor_t<dimensions, a_value_t>;
using a_simplex_shift_params_t  = simplex_shift_params<dimensions, a_value_t>;

using a_nelder_mead_t        = nelder_mead_functor_t<dimensions, a_value_t>;
using a_nelder_mead_params_t = nelder_mead_params<a_value_t>;

using a_random_device_t = std::mt19937;

a_random_device_t get_seeded_rand_dev()
{
    std::random_device rand_dev{};

    return a_random_device_t{ rand_dev() };
}

template <class... methods_t>
using a_simplex_functor_weighted_t = simplex_functor_weighted<dimensions, a_value_t, methods_t...>;

template <std::size_t weigth>
using w_simplex_shift_functor_t = weighted_simplex_functor_wrapper<a_simplex_shift_functor_t, weigth>;
template <std::size_t weigth>
using w_nelder_mead_t = weighted_simplex_functor_wrapper<a_nelder_mead_t, weigth>;

template <std::size_t sshift_weight, std::size_t nelder_mead_weight>
using tanm_round = a_simplex_functor_weighted_t<w_simplex_shift_functor_t<sshift_weight>,
    w_nelder_mead_t<nelder_mead_weight>>;

using round_1_method_t = tanm_round<15, 1>;
using round_2_method_t = tanm_round<5, 1>;
using round_3_method_t = tanm_round<1, 3>;
using round_4_method_t = tanm_round<1, 5>;

using a_iterations_t   = iterations<dimensions, a_value_t>;
using a_target_value_t = target_value<dimensions, a_value_t>;

template <class... functors_t>
using a_combined_termination_functor_t
    = combined_termination_functor<dimensions, a_value_t, functors_t...>;

using a_termination_functor_t = a_combined_termination_functor_t<a_iterations_t, a_target_value_t>;

template <class functor_t>
using a_simplex_adapter_t = simplex_adapter<dimensions, a_value_t, functor_t>;

template <class method_t>
using a_simplex_search_round_t
    = search_round<dimensions, a_value_t, a_simplex_adapter_t<method_t>, a_termination_functor_t>;

using a_threshold_functor_t = default_threshold_functor<a_value_t>;

using round_1_t = a_simplex_search_round_t<round_1_method_t>;
using round_2_t = a_simplex_search_round_t<round_2_method_t>;
using round_3_t = a_simplex_search_round_t<round_3_method_t>;
using round_4_t = a_simplex_search_round_t<round_4_method_t>;

template <int a, int b>
double rosenbrook_function_2d(const a_vector_t& coord)
{
    const double& x = coord[0];
    const double& y = coord[1];
    return std::pow(static_cast<double>(a) - x, 2.0)
        + static_cast<double>(b) * pow((y - pow(x, 2.0)), 2.0);
}

double function(const a_vector_t& coord)
{
    return rosenbrook_function_2d<1, 100>(coord);
}

int main(int argc, char** argv)
{
    a_simplex_t simplex = generate_basic_simplex<dimensions, a_value_t, &function>(25, { 10, 25 });

    round_1_method_t        round_1_method{ a_simplex_shift_params_t{
                                         a_simplex_shift_params_t::shift_distance_t{ -5000.0, 5000.0 },
                                         a_simplex_shift_params_t::shift_distance_t{ -5000.0, 5000.0 } },
        a_nelder_mead_params_t{ 1.0, 2.0, 0.5, 0.5 } };
    a_termination_functor_t term_functor_1{ a_iterations_t{ 10000 }, a_target_value_t{ 100.0 } };
    round_1_t round_1{ a_simplex_adapter_t<round_1_method_t>{ simplex, std::move(round_1_method) },
        std::move(term_functor_1) };

    round_2_method_t        round_2_method{ a_simplex_shift_params_t{
                                         a_simplex_shift_params_t::shift_distance_t{ -50.0, 50.0 },
                                         a_simplex_shift_params_t::shift_distance_t{ -50.0, 50.0 } },
        a_nelder_mead_params_t{ 1.0, 2.0, 0.5, 0.5 } };
    a_termination_functor_t term_functor_2{ a_iterations_t{ 5000 }, a_target_value_t{ 0.1 } };
    round_2_t round_2{ a_simplex_adapter_t<round_2_method_t>{ simplex, std::move(round_2_method) },
        std::move(term_functor_2) };

    round_3_method_t        round_3_method{ a_simplex_shift_params_t{
                                         a_simplex_shift_params_t::shift_distance_t{ -1.0, 1.0 },
                                         a_simplex_shift_params_t::shift_distance_t{ -1.0, 1.0 } },
        a_nelder_mead_params_t{ 1.0, 2.0, 0.5, 0.5 } };
    a_termination_functor_t term_functor_3{ a_iterations_t{ 2000 }, a_target_value_t{ 0.01 } };
    round_3_t round_3{ a_simplex_adapter_t<round_3_method_t>{ simplex, std::move(round_3_method) },
        std::move(term_functor_3) };

    round_4_method_t        round_4_method{ a_simplex_shift_params_t{
                                         a_simplex_shift_params_t::shift_distance_t{ -0.001, 0.001 },
                                         a_simplex_shift_params_t::shift_distance_t{ -0.001, 0.001 } },
        a_nelder_mead_params_t{ 1.0, 2.0, 0.5, 0.5 } };
    a_termination_functor_t term_functor_4{ a_iterations_t{ 5000 }, a_target_value_t{ 1.0e-10 } };
    round_4_t round_4{ a_simplex_adapter_t<round_4_method_t>{ simplex, std::move(round_4_method) },
        std::move(term_functor_4) };

    a_threshold_functor_t threshold_functor{};
    a_random_device_t     rand_dev{ get_seeded_rand_dev() };

    a_iteration_result_t result;

    result = round_1.run_round<function, a_random_device_t, a_threshold_functor_t>(
        {}, rand_dev, threshold_functor);
    print_iteration_result(std::cout, result);

    result = round_2.run_round<function, a_random_device_t, a_threshold_functor_t>(
        {}, rand_dev, threshold_functor);
    print_iteration_result(std::cout, result);

    result = round_3.run_round<function, a_random_device_t, a_threshold_functor_t>(
        {}, rand_dev, threshold_functor);
    print_iteration_result(std::cout, result);

    result = round_4.run_round<function, a_random_device_t, a_threshold_functor_t>(
        {}, rand_dev, threshold_functor);
    print_iteration_result(std::cout, result);
}
