//
// Created by dvl on 9/24/19.
//
#ifndef H_METAHEURISTICS_METAHEURISTICS_HPP
#define H_METAHEURISTICS_METAHEURISTICS_HPP

#include <array>
#include "ct_for_each.hpp"

template <std::size_t dimension, class value_t>
using vector_t = std::array<value_t, dimension>;

template <std::size_t dimension, class value_t>
using function_t = value_t (*)(const vector_t<dimension, value_t>&);

template <std::size_t dimension, class value_t>
struct iteration_result {
    vector_t<dimension, value_t> point;
    value_t                      f_point;
};

template <std::size_t dimension, class value_t>
class search_method_cp_interface {
public:
    using a_iteration_result_t = iteration_result<dimension, value_t>;
    using a_vector_t           = vector_t<dimension, value_t>;

    template <class random_device_t, class threshold_functor_t>
    using args_t
        = std::tuple<a_iteration_result_t&, const a_vector_t&, random_device_t&, threshold_functor_t&>;

    template <function_t<dimension, value_t> function, class random_device_t, class threshold_functor_t>
    struct resolver {
        template <class functor_t>
        using member_fun_t = void (functor_t::*)(a_iteration_result_t&,
            const a_vector_t&,
            random_device_t&,
            threshold_functor_t&);

        template <class functor_t>
        static constexpr member_fun_t<functor_t> resolve(functor_t& functor)
        {
            return &functor_t::
                template perform_iteration<function, random_device_t, threshold_functor_t>;
        }
    };
};

template <std::size_t dimension, class value_t, class search_method_t, class termination_functor_t>
class search_round {
public:
    using a_vector_t = vector_t<dimension, value_t>;

    using a_iteration_result_t = iteration_result<dimension, value_t>;

private:
    search_method_t       _search_method;
    termination_functor_t _termination_functor;

public:
    search_round(search_method_t&& search_method, termination_functor_t&& termination_functor)
        : _search_method{ search_method }
        , _termination_functor{ termination_functor }
    {
    }

    template <function_t<dimension, value_t> function, class random_device_t, class threshold_functor_t>
    a_iteration_result_t run_round(const a_vector_t& vertice,
        random_device_t&                             random_device,
        threshold_functor_t&                         threshold_functor)
    {
        a_iteration_result_t result{};
        a_vector_t           a_vertice{ vertice };
        do {
            _search_method.template perform_iteration<function, random_device_t, threshold_functor_t>(
                result, a_vertice, random_device, threshold_functor);
            a_vertice = result.point;
        } while (!_termination_functor(result));

        return result;
    }
};

#endif // H_METAHEURISTICS_METAHEURISTICS_HPP