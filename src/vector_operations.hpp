//
// Created by dvl on 2/20/20.
//

#ifndef H_METAHEURISTICS_VECTOR_OPERATIONS_HPP
#define H_METAHEURISTICS_VECTOR_OPERATIONS_HPP

#include "metaheuristics.hpp"

template <int dimension, class data_t>
void add_vector_vector(vector_t<dimension, data_t>& to, const vector_t<dimension, data_t>& from)
{
    for (std::size_t element = 0; element < dimension; ++element) {
        to[element] += from[element];
    }
}

template <int dimension, class data_t>
void sub_vector_vector(std::array<data_t, dimension>& to, const std::array<data_t, dimension>& from)
{
    for (std::size_t element = 0; element < dimension; ++element) {
        to[element] -= from[element];
    }
}

template <int dimension, class data_t>
void mul_scalar_vector(data_t scalar, std::array<data_t, dimension>& vector)
{
    for (std::size_t element = 0; element < dimension; ++element) {
        vector[element] = scalar * vector[element];
    }
}

#endif // H_METAHEURISTICS_VECTOR_OPERATIONS_HPP
