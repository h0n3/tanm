//
// Created by dvl on 2/27/20.
//

#ifndef H_METAHEURISTICS_TERMINATION_HPP
#define H_METAHEURISTICS_TERMINATION_HPP

#include "target_value.hpp"
#include "iterations.hpp"

/* This is only for reference
template <std::size_t dimension, class value_t>
class termination_functor_cp_interface {
    using a_iteration_result_t = iteration_result<dimension, value_t>;

    bool operator()(const a_iteration_result_t& vertice) {}
};
*/

template <class value_t>
class evaluator_t {
    const value_t& _value;

public:
    evaluator_t(const value_t& value)
        : _value{ value }
    {
    }

    template <class... functor_list_t>
    bool operator()(functor_list_t... functors)
    {
        return (functors(_value) || ...);
    }
};

template <class value_t, class... functor_list_t>
bool evaluate(const value_t& value, std::tuple<functor_list_t...>& tuple)
{
    using a_evaluator_t = evaluator_t<value_t>;

    a_evaluator_t e_eval{ value };

    return std::apply(e_eval, tuple);
}

template <std::size_t dimension, class value_t, class... functor_list_t>
class combined_termination_functor {
    using a_iteration_result_t = iteration_result<dimension, value_t>;
    using functors_t           = std::tuple<functor_list_t...>;

    functors_t _functors;

public:
    combined_termination_functor(functor_list_t... functors)
        : _functors{ functors... }
    {
    }

    bool operator()(const a_iteration_result_t& vertice)
    {
        return evaluate(vertice, _functors);
    }
};

#endif // H_METAHEURISTICS_TERMINATION_HPP
