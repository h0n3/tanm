//
// Created by dvl on 2/27/20.
//

#ifndef H_METAHEURISTICS_TARGET_VALUE_HPP
#define H_METAHEURISTICS_TARGET_VALUE_HPP

#include "../metaheuristics.hpp"

template <std::size_t dimension, class value_t>
class target_value {
    using a_iteration_result_t = iteration_result<dimension, value_t>;

    const value_t _target_value;

public:
    target_value(value_t value)
        : _target_value{ value }
    {
    }

    bool operator()(const a_iteration_result_t& vertice)
    {
        return vertice.f_point <= _target_value;
    }
};

#endif // H_METAHEURISTICS_TARGET_VALUE_HPP
