//
// Created by dvl on 2/27/20.
//

#ifndef H_METAHEURISTICS_ITERATIONS_HPP
#define H_METAHEURISTICS_ITERATIONS_HPP

template <std::size_t dimension, class value_t>
class iterations {
    using a_iteration_result_t = iteration_result<dimension, value_t>;

    const std::size_t _max_iterations;
    std::size_t       _iteration;

public:
    iterations(std::size_t max_iterations)
        : _max_iterations{ max_iterations }
        , _iteration{ 0 }
    {
    }

    bool operator()(const a_iteration_result_t& vertice)
    {
        return _iteration++ >= _max_iterations;
    }
};

#endif // H_METAHEURISTICS_ITERATIONS_HPP
