#ifndef H_TANM_SIMPLEX_HPP
#define H_TANM_SIMPLEX_HPP

#include "../metaheuristics.hpp"
#include "../tuple_helpers.hpp"
#include "../vector_operations.hpp"

template <std::size_t dimension, class value_t>
struct simplex_vector_t {
    vector_t<dimension, value_t> vertice;
    value_t                      function_value;
};

template <std::size_t dimension, class value_t>
using simplex_t = std::array<simplex_vector_t<dimension, value_t>, dimension + 1>;

template <std::size_t dimension, class value_t>
void sort_vertices(simplex_t<dimension, value_t>& simplex)
{
    using a_simplex_vector_t = simplex_vector_t<dimension, value_t>;

    std::sort(simplex.begin(), simplex.end(), [](a_simplex_vector_t& a, a_simplex_vector_t& b) {
        return a.function_value < b.function_value;
    });
}

template <std::size_t dimension, class value_t, function_t<dimension, value_t> function>
simplex_t<dimension, value_t> generate_basic_simplex(value_t vertice_length,
    vector_t<dimension, value_t>                             offset)
{
    simplex_t<dimension, value_t> result;

    vector_t<dimension, value_t> null_vec;
    for (std::size_t entry = 0; entry < dimension; ++entry) {
        null_vec[entry] = value_t(0);
    }

    for (std::size_t vertice = 0; vertice < dimension; ++vertice) {
        vector_t<dimension, value_t>& a_vertice = result[vertice].vertice;

        a_vertice          = null_vec;
        a_vertice[vertice] = vertice_length;

        add_vector_vector<dimension, value_t>(a_vertice, offset);

        result[vertice].function_value = function(a_vertice);
    }
    vector_t<dimension, value_t>& a_vertice = result[dimension].vertice;
    a_vertice                               = null_vec;
    add_vector_vector<dimension, value_t>(a_vertice, offset);

    result[dimension].function_value = function(a_vertice);

    sort_vertices(result);

    return result;
}

template <std::size_t dimension, class value_t>
class simplex_search_method_cp_interface {
public:
    using a_iteration_result_t = iteration_result<dimension, value_t>;
    using a_simplex_t          = simplex_t<dimension, value_t>;

    template <class random_device_t, class threshold_functor_t>
    using args_t
        = std::tuple<a_iteration_result_t&, a_simplex_t&, random_device_t&, threshold_functor_t&>;

    template <function_t<dimension, value_t> function, class random_device_t, class threshold_functor_t>
    struct resolver {
        template <class functor_t>
        using member_fun_t = void (
            functor_t::*)(a_iteration_result_t&, a_simplex_t&, random_device_t&, threshold_functor_t&);

        template <class functor_t>
        static constexpr member_fun_t<functor_t> resolve(functor_t& functor)
        {
            return &functor_t::
                template perform_iteration<function, random_device_t, threshold_functor_t>;
        }
    };
};

template <std::size_t dimension, class value_t, class simplex_method_t>
class simplex_adapter {
public:
    using a_iteration_result_t = iteration_result<dimension, value_t>;

    using a_vector_t  = vector_t<dimension, value_t>;
    using a_simplex_t = simplex_t<dimension, value_t>;

private:
    simplex_method_t _simplex_method;

    a_simplex_t& _simplex;

public:
    template <class... args_t>
    simplex_adapter(a_simplex_t& simplex, args_t... args)
        : _simplex_method{ args... }
        , _simplex{ simplex }
    {
    }

    template <function_t<dimension, value_t> function, class random_device_t, class threshold_functor_t>
    void perform_iteration(a_iteration_result_t& result,
        const a_vector_t&                        vertice,
        random_device_t&                         random_device,
        threshold_functor_t&                     threshold_functor)
    {
        _simplex_method.template perform_iteration<function, random_device_t, threshold_functor_t>(
            result, _simplex, random_device, threshold_functor);
    }
};

#endif // H_TANM_SIMPLEX_HPP