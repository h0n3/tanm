#ifndef H_TANM_SIMPLEX_SHIFT_HPP
#define H_TANM_SIMPLEX_SHIFT_HPP

#include <random>

#include "simplex.hpp"
#include "../vector_operations.hpp"

template <std::size_t dimension, class value_t>
struct simplex_shift_params {
    using shift_distance_t = std::uniform_real_distribution<value_t>;
    std::array<shift_distance_t, dimension> dimension_shift_distance;
};

template <std::size_t dimension, class value_t>
class simplex_shift_functor_t {
public:
    using a_vector_t  = vector_t<dimension, value_t>;
    using a_simplex_t = simplex_t<dimension, value_t>;

    using a_iteration_result_t     = iteration_result<dimension, value_t>;
    using a_simplex_shift_params_t = simplex_shift_params<dimension, value_t>;

    using a_function_t = function_t<dimension, value_t>;

private:
    a_simplex_shift_params_t _params;

public:
    simplex_shift_functor_t(const a_simplex_shift_params_t& params)
        : _params{ params }
    {
    }

    template <a_function_t function, class random_device_t, class threshold_functor_t>
    void perform_iteration(a_iteration_result_t& result,
        a_simplex_t&                             simplex,
        random_device_t&                         random_device,
        threshold_functor_t&                     threshold_functor)
    {
        a_vector_t shift_vector{};
        for (std::size_t a_dimension = 0; a_dimension < dimension; ++a_dimension) {
            shift_vector[a_dimension] = _params.dimension_shift_distance[a_dimension](random_device);
        }

        a_simplex_t shifted_simplex{ simplex };
        for (auto& vertice : shifted_simplex) {
            add_vector_vector<dimension, value_t>(vertice.vertice, shift_vector);
            vertice.function_value = function(vertice.vertice);
        }
        sort_vertices<dimension, value_t>(shifted_simplex);

        const value_t max_f_old = simplex.front().function_value;
        const value_t max_f_new = shifted_simplex.front().function_value;

        // FIXME is this comparrison ok ???
        if (max_f_new < threshold_functor(max_f_old)) {
            simplex        = shifted_simplex;
            result.f_point = max_f_new;
        } else {
            result.f_point = max_f_old;
        }
        result.point = simplex.front().vertice;
    }
};
#endif // H_TANM_SIMPLEX_SHIFT_HPP