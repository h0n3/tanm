#include <array>

#include "../vector_operations.hpp"
#include "simplex.hpp"

template <class value_t>
struct nelder_mead_params {
    value_t rho;
    value_t psi;
    value_t xi;
    value_t sigma;
};

/* This is only a reference. Could be translated to a c++20 concept.
template<std::size_t dimension, class value_t, function_t<dimension, value_t> function, class
iteration_data_t > struct iteration_data_cp_interface{ using a_vector_t = vector_t<dimension,value_t>;

    iteration_data_cp_interface(const nm_params, const simplex_t<dimension>&);

    a_vector_t& mean();
    a_vector_t& reflection();
    a_vector_t& expansion();
    a_vector_t& out_contraction();
    a_vector_t& in_contraction();
};
*/

template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calculate_mean(const simplex_t<dimension, value_t>& simplex)
{
    vector_t<dimension, value_t> result{ dimension, 0.0 };
    for (std::size_t vertice = 0; vertice < dimension; ++vertice) {
        add_vector_vector<dimension, double>(result, simplex[vertice].vertice);
    }
    mul_scalar_vector<dimension, double>(1.0 / dimension, result);
    return result;
}

template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calc_weighted_vertice_displacement(double weight,
    const vector_t<dimension, value_t>&                                mean,
    const vector_t<dimension, value_t>&                                top_vertice)
{
    vector_t<dimension, value_t> scaled_top_vertice{ top_vertice };
    mul_scalar_vector<dimension, double>(weight, scaled_top_vertice);

    vector_t<dimension, value_t> result{ mean };
    mul_scalar_vector<dimension, double>((1.0 + weight), result);

    sub_vector_vector<dimension, double>(result, scaled_top_vertice);
    return result;
}

template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calculate_reflection(const nelder_mead_params<value_t>& params,
    const vector_t<dimension, value_t>&                                              mean,
    const vector_t<dimension, value_t>&                                              top_vertice)
{
    return calc_weighted_vertice_displacement<dimension>(params.rho, mean, top_vertice);
}
template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calculate_expansion(const nelder_mead_params<value_t>& params,
    const vector_t<dimension, value_t>&                                             mean,
    const vector_t<dimension, value_t>&                                             top_vertice)
{
    return calc_weighted_vertice_displacement<dimension>(params.rho * params.xi, mean, top_vertice);
}
template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calculate_out_contraction(const nelder_mead_params<value_t>& params,
    const vector_t<dimension, value_t>&                                                   mean,
    const vector_t<dimension, value_t>&                                                   top_vertice)
{
    return calc_weighted_vertice_displacement<dimension>(params.rho * params.psi, mean, top_vertice);
}
template <std::size_t dimension, class value_t>
vector_t<dimension, value_t> calculate_in_contraction(const nelder_mead_params<value_t>& params,
    const vector_t<dimension, value_t>&                                                  mean,
    const vector_t<dimension, value_t>&                                                  top_vertice)
{
    return calc_weighted_vertice_displacement<dimension>(
        params.rho * params.psi * -1.0, mean, top_vertice);
}

template <std::size_t dimension, class value_t>
using calc_new_vertice_function_t = vector_t<dimension, value_t> (*)(const nelder_mead_params<value_t>&,
    const vector_t<dimension, value_t>&,
    const vector_t<dimension, value_t>&);

template <std::size_t dimension,
    class value_t,
    function_t<dimension, value_t>                  function,
    calc_new_vertice_function_t<dimension, value_t> calc_new_vertice_function>
simplex_vector_t<dimension, value_t> calc_vertice_and_function_value(
    const nelder_mead_params<value_t>&  params,
    const vector_t<dimension, value_t>& mean,
    const vector_t<dimension, value_t>& top)
{
    simplex_vector_t<dimension, value_t> result{ calc_new_vertice_function(params, mean, top),
        value_t{} };

    result.function_value = function(result.vertice);

    return result;
}

template <std::size_t dimension, class value_t, function_t<dimension, value_t> function>
struct iteration_data_static {
    using a_simplex_vector_t = simplex_vector_t<dimension, value_t>;

    using a_calc_new_vertice_function_t = calc_new_vertice_function_t<dimension, value_t>;
    template <a_calc_new_vertice_function_t a_calc_new_vertice_function>
    a_simplex_vector_t calc_simplex_vertice(const nelder_mead_params<value_t>& params,
        const vector_t<dimension, value_t>&                                    mean,
        const vector_t<dimension, value_t>&                                    top)
    {
        return calc_vertice_and_function_value<dimension,
            value_t,
            function,
            a_calc_new_vertice_function>(params, mean, top);
    }

    a_simplex_vector_t _mean;
    a_simplex_vector_t _reflection;
    a_simplex_vector_t _expansion;
    a_simplex_vector_t _out_contraction;
    a_simplex_vector_t _in_contraction;

public:
    iteration_data_static(const nelder_mead_params<value_t> params,
        const simplex_t<dimension, value_t>&                simplex)
    {
        _mean.vertice = calculate_mean<dimension, value_t>(simplex);
        // FIXME is this(_mean.function_value) value ever used ?
        _mean.function_value = function(_mean.vertice);

        const auto& top_vertice{ simplex.front().vertice };
        const auto& mean_vertice{ _mean.vertice };

        _reflection = calc_simplex_vertice<calculate_reflection>(params, mean_vertice, top_vertice);
        _expansion  = calc_simplex_vertice<calculate_expansion>(params, mean_vertice, top_vertice);
        _out_contraction
            = calc_simplex_vertice<calculate_out_contraction>(params, mean_vertice, top_vertice);
        _in_contraction
            = calc_simplex_vertice<calculate_in_contraction>(params, mean_vertice, top_vertice);
    }

    const a_simplex_vector_t& mean()
    {
        return _mean;
    }
    const a_simplex_vector_t& reflection()
    {
        return _reflection;
    }
    const a_simplex_vector_t& expansion()
    {
        return _expansion;
    }
    const a_simplex_vector_t& out_contraction()
    {
        return _out_contraction;
    }
    const a_simplex_vector_t& in_contraction()
    {
        return _in_contraction;
    }

private:
};

template <std::size_t dimension, class value_t, function_t<dimension, value_t> function>
void shrink(const nelder_mead_params<value_t>& params, simplex_t<dimension, value_t>& simplex)
{
    const vector_t<dimension, value_t>& first_vertice = simplex.front().vertice;
    for (std::size_t vertice = 1; vertice < simplex.size(); ++vertice) {
        vector_t<dimension, value_t>& active_vertice = simplex[vertice].vertice;
        // gepimter algo ohne allocations :P
        sub_vector_vector<dimension, value_t>(active_vertice, first_vertice); // a_v = (a_v - f_v)
        mul_scalar_vector<dimension, value_t>(
            params.sigma * -1.0, active_vertice); // a_v = -sigma(a_v - f_v)
        add_vector_vector<dimension, value_t>(
            active_vertice, first_vertice); // a_v = -sigma(a_v - f_v) + f_v

        simplex[vertice].function_value = function(active_vertice);
    }
}

template <std::size_t dimension, class value_t>
class nelder_mead_functor_t {
public:
    using a_vector_t         = vector_t<dimension, value_t>;
    using a_simplex_t        = simplex_t<dimension, value_t>;
    using a_simplex_vector_t = simplex_vector_t<dimension, value_t>;

    using a_iteration_result_t = iteration_result<dimension, value_t>;
    using a_nm_params_t        = nelder_mead_params<value_t>;

    using a_function_t = function_t<dimension, value_t>;

private:
    a_nm_params_t _params;

public:
    nelder_mead_functor_t(a_nm_params_t params)
        : _params{ params }
    {
    }

    template <a_function_t function, class random_device_t, class threshold_functor_t>
    void perform_iteration(a_iteration_result_t& result,
        a_simplex_t&                             simplex,
        random_device_t&                         random_device,
        threshold_functor_t&                     threshold_functor)
    {
        using iteration_data_t = iteration_data_static<dimension, value_t, function>;
        iteration_data_t iteration_data{ _params, simplex };

        bool               do_shrink{ false };
        a_simplex_vector_t new_vertice;

        value_t f_reflection = iteration_data.reflection().function_value;

        value_t f_x1 = simplex.front().function_value;
        if (f_reflection < threshold_functor(f_x1)) {
            const value_t f_expansion = iteration_data.expansion().function_value;
            if (f_expansion < threshold_functor(f_reflection)) {
                new_vertice = iteration_data.expansion();
            } else {
                new_vertice = iteration_data.reflection();
            }
        } else {
            const value_t f_xn = simplex[dimension].function_value;
            if (f_reflection < threshold_functor(f_xn)) {
                new_vertice = iteration_data.reflection();
            } else {
                const value_t f_xn1 = simplex.back().function_value;
                if (f_reflection < threshold_functor(f_xn1)) {
                    auto f_xo = iteration_data.out_contraction().function_value;
                    if (f_xo < threshold_functor(f_xn1)) {
                        new_vertice = iteration_data.out_contraction();
                    } else {
                        do_shrink = true;
                    }
                } else {
                    const value_t f_xi = iteration_data.in_contraction().function_value;
                    if (f_xi < threshold_functor(f_xn1)) {
                        new_vertice = iteration_data.in_contraction();
                    } else {
                        do_shrink = true;
                    }
                }
            }
        }

        if (do_shrink) {
            shrink<dimension, value_t, function>(_params, simplex);
        } else {
            simplex.back() = new_vertice;
        }

        sort_vertices<dimension, value_t>(simplex);

        result.point   = simplex.front().vertice;
        result.f_point = simplex.front().function_value;
    }
};