//
// Created by dvl on 2/24/20.
//

#ifndef H_METAHEURISTICS_SIMPLEX_FUNCTOR_WEIGHTED_HPP
#define H_METAHEURISTICS_SIMPLEX_FUNCTOR_WEIGHTED_HPP

template <class... param_pack_t>
constexpr std::size_t get_weight_sum()
{
    return (param_pack_t::weight + ...);
}

template <class impl, std::size_t operation_weigth>
class weighted_simplex_functor_wrapper : private impl {
public:
    static constexpr const std::size_t weight = operation_weigth;

    using a_iteration_result_t = typename impl::a_iteration_result_t;

    using a_vector_t  = typename impl::a_vector_t;
    using a_simplex_t = typename impl::a_simplex_t;

    using a_function_t = typename impl::a_function_t;

    template <class... args_t>
    weighted_simplex_functor_wrapper(args_t... args)
        : impl{ args... }
    {
    }

    template <a_function_t function, class random_device_t, class threshold_functor_t>
    void perform_iteration(a_iteration_result_t& result,
        a_simplex_t&                             simplex,
        random_device_t&                         random_device,
        threshold_functor_t&                     threshold_functor)
    {
        return impl::template perform_iteration<function, random_device_t, threshold_functor_t>(
            result, simplex, random_device, threshold_functor);
    }
};

template <std::size_t dimension, class value_t, class... simplex_functors_t>
class simplex_functor_weighted {
    static constexpr const std::size_t _weigth_sum = get_weight_sum<simplex_functors_t...>();

public:
    using a_iteration_result_t = iteration_result<dimension, value_t>;

    using a_vector_t  = vector_t<dimension, value_t>;
    using a_simplex_t = simplex_t<dimension, value_t>;

    using functor_list_t = std::tuple<simplex_functors_t...>;

    using a_function_t = function_t<dimension, value_t>;

    template <class resolver_t>
    using functor_list_accessor_t = tuple_by_id_index<resolver_t, simplex_functors_t...>;

private:
    functor_list_t                             _functor_list;
    std::uniform_int_distribution<std::size_t> _distribution;

public:
    simplex_functor_weighted(simplex_functors_t... simplex_functors)
        : _functor_list{ simplex_functors... }
        , _distribution{ 1, _weigth_sum }
    {
    }

    template <a_function_t function, class random_device_t, class threshold_functor_t>
    void perform_iteration(a_iteration_result_t& result,
        a_simplex_t&                             simplex,
        random_device_t&                         random_device,
        threshold_functor_t&                     threshold_functor)
    {
        using a_search_method_cp_interface_t = simplex_search_method_cp_interface<dimension, value_t>;
        using a_resolver_t                   = typename a_search_method_cp_interface_t::
            template resolver<function, random_device_t, threshold_functor_t>;
        using a_functor_list_accessor_t = functor_list_accessor_t<a_resolver_t>;

        constexpr const std::size_t num_functors = sizeof...(simplex_functors_t);
        static_assert(num_functors, "Missing functor");

        std::size_t functor_id = 0;
        if constexpr (num_functors > 1) {
            // throw dice
            auto p = _distribution(random_device);

            // retrieve the responsible functor for that range and invoke it
            functor_id = get_entry_id<simplex_functors_t...>(p);
        }

        a_functor_list_accessor_t::template execute_at<a_iteration_result_t&,
            a_simplex_t&,
            random_device_t&,
            threshold_functor_t&>(
            functor_id, _functor_list, result, simplex, random_device, threshold_functor);
    }
};

#endif // H_METAHEURISTICS_SIMPLEX_FUNCTOR_WEIGHTED_HPP
